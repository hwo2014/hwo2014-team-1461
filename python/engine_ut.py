import collections
import unittest

import engine


class EngineTest(unittest.TestCase):

    def setUp(self):
        self.eng = engine.Engine()
        
    def test_parses_track_with_one_element(self):
        self.assertEqual(0, 0)
        
class PidEngineTest(unittest.TestCase):

    def setUp(self):
        self.eng = engine.PidEngine()
        
    def test_angle_circular_buffer(self):
        for angle in range(10):
            self.eng.store_err(angle)
        self.assertEqual(collections.deque(range(5, 10), maxlen=5), self.eng.err_cb)
        
    def test_derivative_on_empty_cb(self):
        self.assertEqual(0.0, self.eng.derivative())
        
    def test_derivative_of_one_value(self):
        self.eng.store_err(2)
        self.assertEqual(2.0, self.eng.derivative())
        
    def test_derivative(self):
        self.eng.store_err(2)
        self.eng.store_err(5)
        self.assertEqual(3, self.eng.derivative())
        
    def test_integral_on_empty_cb(self):
        self.assertEqual(0.0, self.eng.integral())
        
    def test_integral_of_ones(self):
        for i in range(5):
            self.eng.store_err(1)
        self.assertEqual(1.0, self.eng.integral())
        
    def test_pid(self):
        for i in range(5):
            self.eng.store_err(1)
        self.assertEqual(1.0, self.eng.run_pid())
        
    def test_throttle_limit(self):
        self.assertEqual(0.0, self.eng.limit_throttle(-1.2))
        self.assertEqual(1.0, self.eng.limit_throttle(1.9))
        
    def test_angle_to_throttle(self):
        self.assertEqual(0.0, self.eng.angle_to_throttle(0))
        self.assertEqual(1.0, self.eng.angle_to_throttle(90))
        self.assertEqual(1.0, self.eng.angle_to_throttle(-90))
        
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
    