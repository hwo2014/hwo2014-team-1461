import collections

class Engine(object):
    
    def on_car_positions(self, data):        
        throttle = 0.6
        return throttle
    
    def on_game_init(self, data):
        self.parse_track_info(data)
        
    def parse_track_info(self, data):
        strategy = []
        pieces = data['race']['track']['pieces']
        for piece in pieces:
            strategy.append(self.get_strategy(piece))
        return strategy
   
    def get_strategy(self, piece):
        try:
            piece['angle']
        except KeyError:
            return 'str'
        return 'crv'
        
        
class PidEngine(Engine):
    
    def __init__(self):
        self.k      = [0.7, 0.1, 0.3]
        self.abs_sp = 15.0
        self.dt     = 15
        self.err_cb = collections.deque([0.0]*self.dt, maxlen=self.dt)
    
    def on_car_positions(self, data):        
        angle = self.get_angle(data)
        self.store_err(self.error(angle))
        cv = self.run_pid()
        throttle = self.angle_to_throttle(angle, cv)
        return throttle

    def get_angle(self, data):
        return data[0]['angle']
    
    def store_err(self, angle):
        self.err_cb.append(angle)
        
    def error(self, pv):
        sp = self.sign(pv)*self.abs_sp
        return sp - pv
    
    def run_pid(self):
        err      = self.err_cb[-1]
        der      = self.derivative()
        integral = self.integral()
        return self.k[0]*err + self.k[1]*der + self.k[2]*integral 
    
    def derivative(self):
        return self.err_cb[-1] - self.err_cb[-2]
    
    def integral(self):
        return sum(self.err_cb)/self.dt
    
    def angle_to_throttle(self, angle, cv):
        throttle = 0.75 - self.limit_angle(abs(cv))/90;
        return max(throttle, 0.0)
    
    def limit_angle(self, angle):
        return max(min(angle, 90.0), 0.0)
    
    def sign(self, val):
        sign = 1
        if val < 0:
            sign = -1
        return sign
