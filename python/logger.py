import time

class Logger(object):
    
    def __init__(self):
        self.logger_enabled = False
        if self.logger_enabled is False:
            return
        date = time.strftime("%d_%H_%M_%S")
        log_dir = "../logs/"
        self.log_file = open(log_dir + date + "_log.log", "w")
        self.speed_log_file = open(log_dir + date + "_speed.log", "w")
        
        
    def log(self, msg_type, data):
        if self.logger_enabled is False:
            return
        
        logger_map = {'gameInit'     : self.log_track,
                      'carPositions' : self.log_position,
                      'throttle'     : self.log_speed}
        
        try:
            logger_map[msg_type](data)
        except KeyError:
            pass
        
    def log_track(self, data):
        return "track info placeholder\n"
    
    def log_position(self, data):
        angle           = data[0]['angle']
        piecePosition   = data[0]['piecePosition']
        pieceIndex      = piecePosition['pieceIndex']
        inPieceDistance = piecePosition['inPieceDistance']
        lap             = piecePosition['lap']
        record =  str(angle) + " " + str(pieceIndex) + " " + \
            str(inPieceDistance) + " " + str(lap) + "\n"
        self.log_file.write(record)
            
    def log_speed(self, data):
        record = str(data) + "\n" 
        self.speed_log_file.write(record)
    